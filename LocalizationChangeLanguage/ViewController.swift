//
//  ViewController.swift
//  LocalizationChangeLanguage
//
//  Created by Sophealey on 12/6/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myButton: UIButton!
    var myLabel:UILabel?
    override func viewDidLoad() {
        super.viewDidLoad()
        myButton.setTitle("myButton".localized, for: .normal)
        myLabel = UILabel(frame: CGRect(x: 150, y: 100, width: 100, height: 20))
        myLabel?.text = "hello".localized
        self.view.addSubview(myLabel!)
    }

    @IBAction func changeButton(_ sender: UIButton) {
        if UserDefaults.standard.string(forKey: "LanguageCode") == "en"{
            LanguageManager.shared.language = "km"
            UserDefaults.standard.set(["km"], forKey: "AppleLanguages")
  
            
        }else if UserDefaults.standard.string(forKey: "LanguageCode") == "km"{
            LanguageManager.shared.language = "en"
            UserDefaults.standard.set(["en"], forKey: "AppleLanguages")

        }
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let homeController = storyBoard.instantiateInitialViewController()
        appDelegate?.window?.rootViewController = homeController
    }
}

